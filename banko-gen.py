#!/usr/bin/python3

import random
import sys
import os
import xml.etree.ElementTree as ET
import xml.parsers.expat as expat
import cairosvg
import yaml

ET.register_namespace('',"http://www.w3.org/2000/svg")

class BingoCard:
    def __init__(self, number_of_rows=3, seed=None):

        if not seed:
            random.seed()
            seed = random.randrange(0,1000000)
        self.seed = seed

        random.seed(self.seed)

        # Lav en liste af brugbare tal og bland den
        numbers = list(range(1,91))
        random.shuffle(numbers)

        self.rows = []

        for i in range(0,number_of_rows):
            row = ['']*9
            while self._count_row_numbers(row) < 5:
                number = numbers.pop()
                bucket = min(8,number//10)
                if not row[bucket]:
                    row[bucket] = str(number)
            self.rows.append(row)

    def _count_row_numbers(self, row):
        i = 0
        for num in row:
            if num:
                i += 1
        return i

    def get_rows(self):
        return self.rows

def _number2xml(number, size_x, size_y, font_style, font_yadjust, do_rects=False):
    group = ET.Element('g')
    if do_rects:
        rect = ET.SubElement(group, 'rect')
        rect.attrib['width'] = str(size_x)
        rect.attrib['height'] = str(size_y)
        rect.attrib['style'] = 'fill:#fefefe;fill-opacity:1;stroke:#000000;stroke-width:1;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1'
    text = ET.SubElement(group, 'text')
    tspan = ET.SubElement(text, 'tspan')
    tspan.attrib['x'] = str(size_x/2)
    tspan.attrib['y'] = str(font_yadjust)
    tspan.attrib['style'] = font_style
    tspan.text = str(number)
    return group

def _row2xml(row, size_x, size_y, space, font_style, font_yadjust, do_rects):
    i = 0
    row_group = ET.Element('g')
    for number in row:
        group = _number2xml(number, size_x, size_y, font_style, font_yadjust, do_rects)
        group.attrib['transform'] = 'translate({} 0)'.format((space+size_x)*i)
        row_group.append(group)
        i = i + 1
    return row_group

def get_svg_xml(card, size_x=18, size_y=18, space=0,
                font_style="font-style:normal;font-variant:normal;font-weight:bold;" + \
                           "font-stretch:normal;font-size:10;font-family:Cantarell;" + \
                           "-inkscape-font-specification:'Cantarell, Bold';" + \
                           "font-variant-ligatures:normal;font-variant-caps:normal;" + \
                           "font-variant-numeric:normal;font-feature-settings:normal;" + \
                           "text-align:center;writing-mode:lr-tb;text-anchor:middle", \
                font_yadjust=11.5,
                do_seed=False,
                seed_font_style='',
                seed_x=0,
                seed_y=0,
                do_rects=False,
                rect_style=''):
                
    card_group = ET.Element('g')
    i = 0
    for row in card.rows:
        row_group = _row2xml(row, size_x, size_y, space, font_style, font_yadjust, do_rects)
        row_group.attrib['transform'] = 'translate(0 {})'.format((space+size_y)*i)
        card_group.append(row_group)
        i = i + 1
    if do_seed:
        seed_text = ET.SubElement(card_group, 'text')
        tspan = ET.SubElement(seed_text, 'tspan')
        tspan.attrib['x'] = str(seed_x)
        tspan.attrib['y'] = str(seed_y)
        tspan.attrib['style'] = seed_font_style
        tspan.text = "{0:06d}".format(card.seed)

    return card_group

def get_template_pdf(template_filename):
    with open(template_filename,'r') as templatefile:
        template = yaml.load(templatefile.read(), Loader=yaml.Loader)
    
    template_dir = os.path.dirname(os.path.realpath(template_filename))

    tree = ET.parse(template_dir + os.path.sep + template['source_file'])
    for el in tree.findall('{http://www.w3.org/2000/svg}g'):
        if 'id' in el.keys() and el.attrib['id'] == 'layer1':
            global layer1
            layer1 = el
    if layer1 == None:
        print('No layer1!')
        sys.exit(1)

    for i in range(0, template['number_of_cards']):
        card_xml = get_svg_xml(BingoCard(),
                               size_x = template['square_size_x'],
                               size_y = template['square_size_y'],
                               space = template['square_space'],
                               font_style = template['font_style'],
                               font_yadjust=template['font_yadjust'],
                               do_seed = template['do_seed'],
                               seed_x = template['seed_offset_x'],
                               seed_y = template['seed_offset_y'],
                               seed_font_style = template['font_style_seed'],
                               do_rects = template['do_rects']
                               )
        card_xml.attrib['transform'] = "translate({} {})".format(template['positions'][i]['first_row_x'], template['positions'][i]['first_col_y'])
        layer1.append(card_xml)

    with open('test.svg','wb') as f:
        f.write(ET.tostring(tree.getroot()))

    return cairosvg.svg2pdf(bytestring=ET.tostring(tree.getroot()))

def application(environ, start_response):
    output = get_template_pdf('templates/template_01.yaml')
    start_response("200 OK", [
        ("Content-Type","application/pdf"),
        ("Content-Length", str(len(output)))
    ])

    return iter([output])

if __name__ == '__main__':
    with open('output.pdf','wb') as f:
        f.write(get_template_pdf('templates/template_01.yaml'))
