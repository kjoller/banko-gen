# Banko generator

A python experiment to generate british style 90-number bingo cards.

In Danish "Bingo" is US-style 75-number bingo, whereas the british style is
called "Banko", hence the name of this project.

## Requirements

* gunicorn

* cairosvg

Also requires cairo, libffi and of course python3

## Usage: command-line

Just run it:

```sh
python3 banko-gen.py
```

It will read `template.svg` (a basic svg file) and output to `output.pdf`

## Usage: web server

To start a gunicorn webserver:

```sh
gunicorn -w4 banko-gen:application
```
