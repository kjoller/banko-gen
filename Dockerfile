FROM python:3.13-alpine
COPY . /app

RUN mv /app/fonts /usr/share

WORKDIR /app

RUN apk add --no-cache cairo ttf-dejavu

RUN pip install -r requirements.txt

EXPOSE 8000

ENTRYPOINT ["gunicorn"]
CMD ["-b", "0.0.0.0:8000", "banko-gen:application"]
